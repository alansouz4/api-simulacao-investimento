package br.com.carteira.investimento.controllers;

import br.com.carteira.investimento.models.Investimento;
import br.com.carteira.investimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento criaInvestimento(@RequestBody @Valid Investimento investimento){
        Investimento investimentoObjeto = investimentoService.salvarInvestimento(investimento);
        return investimentoObjeto;
    }

    @GetMapping
    public Iterable<Investimento> buscarInvestimentos(){
        Iterable<Investimento> investimentos = investimentoService.buscarInvestimento();
        return investimentos;
    }

}
