package br.com.carteira.investimento.controllers;

import br.com.carteira.investimento.models.Simulacao;
import br.com.carteira.investimento.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Simulacao criarSimulacao(@Valid @RequestBody Simulacao simulacao){
        Simulacao simulacaoObjeto = simulacaoService.salvarSimulacao(simulacao);
        return simulacaoObjeto;
    }

    @GetMapping
    public Iterable<Simulacao> listarSimulacao(){
        Iterable<Simulacao> simulacao = simulacaoService.buscarSimulacao();
        return simulacao;
    }


}
