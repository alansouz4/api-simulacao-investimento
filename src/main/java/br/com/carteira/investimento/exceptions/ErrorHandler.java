package br.com.carteira.investimento.exceptions;

import br.com.carteira.investimento.exceptions.errors.MensagemDeErro;
import br.com.carteira.investimento.exceptions.errors.ObjetoErro;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.HashMap;
import java.util.List;

@ControllerAdvice
public class ErrorHandler {

    // RESPOSTA DE ERRO TRATADO NO PROPERTIES
    @Value("${mensagem.padrao.de.validacao}")
    private String mensagemPadrao;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(code = HttpStatus.UNPROCESSABLE_ENTITY)   // RESPOSTA PADRÃO DO PROCESSAMENTO
    @ResponseBody // RETORNA A RESPOSTA NO CORPO
    public MensagemDeErro menipulacaoDeErrosDeValidacao(MethodArgumentNotValidException exception){
        HashMap<String, ObjetoErro> erros = new HashMap<>();
        BindingResult resultado = exception.getBindingResult(); // PEGANDO O OBJETO getBindingResult DO MÉTODO MethodArgumentNotValidException
        List<FieldError> fieldErrors = resultado.getFieldErrors(); // BINDING ARMAZENADO EM UMA LISTA

        // PERCORRENDO A LISTA
        for(FieldError erro : fieldErrors){
            erros.put(erro.getField(), new ObjetoErro(erro.getDefaultMessage(),
                    erro.getRejectedValue().toString()));
        }

        // INICIANDO OBJETO DA CLASSE mensagemDeErro TRAZENDO OS STATUS, AS MENSAGENS E OS ERROS
        MensagemDeErro mensagemDeErro = new MensagemDeErro(HttpStatus.UNPROCESSABLE_ENTITY.toString(),
                mensagemPadrao, erros);
        return mensagemDeErro;
    }
}
