package br.com.carteira.investimento.models.DTOs;

public class ResultadoSimulacaoDTO {

    private double rendimentoPorMes;
    private String montante;


    public ResultadoSimulacaoDTO(){
    }


    public double getRendimentoPorMes() {
        return rendimentoPorMes;
    }

    public void setRendimentoPorMes(double rendimentoPorMes) {
        this.rendimentoPorMes = rendimentoPorMes;
    }

    public String getMontante() {
        return montante;
    }

    public void setMontante(String montante) {
        this.montante = montante;
    }
}
