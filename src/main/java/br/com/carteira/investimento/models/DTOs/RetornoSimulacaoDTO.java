package br.com.carteira.investimento.models.DTOs;

public class RetornoSimulacaoDTO {

    private String nomeInteressado;
    private String email;
    private Double valorAplicado;
    private int quantidadeMeses;
    private long investimentoId;


    public RetornoSimulacaoDTO(){
    }


    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(Double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadeMeses() {
        return quantidadeMeses;
    }

    public void setQuantidadeMeses(int quantidadeMeses) {
        this.quantidadeMeses = quantidadeMeses;
    }

    public long getInvestimentoId() {
        return investimentoId;
    }

    public void setInvestimentoId(long investimentoId) {
        this.investimentoId = investimentoId;
    }
}
