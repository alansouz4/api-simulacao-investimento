package br.com.carteira.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long idInvestimento;

    @Column(unique = true)
    @Size(min = 5, max = 20, message = "Nome deve ter entre 5 e 20 caracteres")
    @NotNull(message = "Nome não pode ser nulo")
    private String nome;

    @NotNull(message = "Campo não pode ser nulo")
    private double rendimentoAoMes;

    public Investimento(long idInvestimento, String nome, double rendimentoAoMes) {
        this.idInvestimento = idInvestimento;
        this.nome = nome;
        this.rendimentoAoMes = rendimentoAoMes;
    }

    public Investimento(){
    }


    public String getNome() {
        return nome;
    }
    public void setNome(String nome) {
        this.nome = nome;
    }
    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }
    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
    public long getIdInvestimento() {
        return idInvestimento;
    }
    public void setIdInvestimento(long idInvestimento) {
        this.idInvestimento = idInvestimento;
    }
}
