package br.com.carteira.investimento.models;

import javax.persistence.*;
import javax.validation.constraints.*;
import java.util.List;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long IdSimulacao;

    @Size(min = 4, max = 50, message = "O nome deve ter entre 4 e 5 caracteres")
    @NotNull(message = "O campo nome não pode ser nulo")
    private String nomeInteressado;

    @Email(message = "Email esta com a sintaxê incorreta")
    @NotNull(message = "O campo email não pode ser nulo")
    private String email;

    @NotNull(message = "O campo valor aplicado não pode ser nulo")
    private double valorAplicado;

    @NotNull(message = "O campo quantidade de meses não pode ser nulo")
    private int quantidadesMeses;

    @ManyToMany(cascade = CascadeType.ALL)
    private List<Investimento> investimentos;

    public Simulacao(){
    }


    public Long getIdSimulacao() {
        return IdSimulacao;
    }

    public void setIdSimulacao(Long idSimulacao) {
        IdSimulacao = idSimulacao;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getValorAplicado() {
        return valorAplicado;
    }

    public void setValorAplicado(double valorAplicado) {
        this.valorAplicado = valorAplicado;
    }

    public int getQuantidadesMeses() {
        return quantidadesMeses;
    }

    public void setQuantidadesMeses(int quantidadesMeses) {
        this.quantidadesMeses = quantidadesMeses;
    }

    public List<Investimento> getInvestimentos() {
        return investimentos;
    }

    public void setInvestimentos(List<Investimento> investimentos) {
        this.investimentos = investimentos;
    }
}
