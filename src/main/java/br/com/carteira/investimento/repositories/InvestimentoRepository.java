package br.com.carteira.investimento.repositories;


import br.com.carteira.investimento.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento, Long> {
}
