package br.com.carteira.investimento.repositories;

import br.com.carteira.investimento.models.Simulacao;
import org.springframework.data.repository.CrudRepository;

public interface SimulacaoRepository extends CrudRepository<Simulacao, Long> {
    Simulacao findByEmail(String email);
}
