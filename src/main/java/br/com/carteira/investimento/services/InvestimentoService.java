package br.com.carteira.investimento.services;

import br.com.carteira.investimento.models.Investimento;
import br.com.carteira.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento salvarInvestimento(Investimento investimento){
        Investimento investimentoObjeto = investimentoRepository.save(investimento);
        return investimentoObjeto;
    }

    public Iterable<Investimento> buscarInvestimento(){
        Iterable<Investimento> investimentos = investimentoRepository.findAll();
        return investimentos;
    }

    public List<Investimento> buscarTodosPorId(List<Long> idInvestimento){
        Iterable<Investimento> investimento = investimentoRepository.findAllById(idInvestimento);
        return (List) investimento;
    }

}
