package br.com.carteira.investimento.services;

import br.com.carteira.investimento.models.Investimento;
import br.com.carteira.investimento.models.Simulacao;
import br.com.carteira.investimento.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SimulacaoService {

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    public Simulacao salvarSimulacao(Simulacao simulacao){

        List<Long> idInv = new ArrayList<>();
        for(Investimento investimento : simulacao.getInvestimentos()){
            Long idInvestimento = investimento.getIdInvestimento();
            idInv.add(idInvestimento);
        }

        // verifica se email já não existe
        Simulacao emailExiste = simulacaoRepository.findByEmail(simulacao.getEmail());

        if(emailExiste != null && !emailExiste.equals(simulacao)){
            throw new RuntimeException("Já existe um cliente cadastrado com este e-mail.");
        }

        List<Investimento> investimentos = investimentoService.buscarTodosPorId(idInv);
        simulacao.setInvestimentos(investimentos);

        Simulacao simulacaoObjeto = simulacaoRepository.save(simulacao);
        return simulacaoObjeto;
    }


    public Iterable<Simulacao> buscarSimulacao(){
        Iterable<Simulacao> simulacao = simulacaoRepository.findAll();
        return simulacao;
    }
}
